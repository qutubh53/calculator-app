import React, { Component } from 'react';
import './Button.css'

class Button extends Component {
	operator = event => {
		return event === "+" || event === "−" || event === "×" || event === "÷"	
	}
	functions = event => {
		return event === "AC" || event === "±" || event === "%" || event === "←"
	}

	handerClick = () => {
		return (
			this.props.onButtonClicked(this.props.name),
			this.operator(this.props.name)
		)
	}
	render(){
		const buttonClass = [
			this.operator(this.props.name) ? 'operator' : '',
			this.functions(this.props.name) ? 'functions' : ''
		]
		return (
			<button
				type="button" 
				className={`num-btn ${buttonClass.join("")}`}
				onClick={this.handerClick}
			>{this.props.name}</button>
		)
	}
}

export default Button;