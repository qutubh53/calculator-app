import React, { Component } from 'react';
import Button from './component/Button';
import TopBar from './Assets/Images/top-bar.png';
import './App.css';

class App extends Component {
	state = {
		input: "",
		beforeOperateValue: "",
		beforeCalculateValue: "",
		operator: ""
	}

	clickHandlerPressed = event => {
		if(event === "AC") {
			this.setState({
				input: "",
				beforeOperateValue: "",
			})
			return
		}

		if(event === "±") {
			this.setState({input: -(this.state.input)})
			return
		}

		if(event === "%") {
			this.setState({input: this.state.input / 100})
			return
		}

		if(event === "←") {
			this.setState({input: this.state.input.slice(0, -1)})
			return
		}

		if(event === ".") {
			if(this.state.input.includes(".")) {
				return null
			}
			else{
				this.setState({input: this.state.input + event})
				return
			}
		}

		if(event === "0") {
			if(this.state.input === "") {
				return null
			}
		}

		if(event === "+") {
			// this.state.beforeOperateValue = this.state.input;
			// this.setState({input: ""})
			// this.state.operator = "+";
			// console.log(this.state.beforeOperateValue);
			// return

			// if(event === "+") {
			// 	console.log("aaaa");
			// 	return
			// }

			// if(this.state.input === "") {
			// 	return null
			// }


		}

		if(event === "−") {
			this.setState({
				beforeOperateValue: this.state.input,
				input: "",
				operator: "−"
			})
			return

			// if(this.state.input === "") {
			// 	return null
			// }
		}

		if(event === "×") {
			this.setState({
				beforeOperateValue: this.state.input,
				input: "",
				operator: "×"
			})
			return

			// if(this.state.input === "") {
			// 	return null
			// }
		}

		if(event === "÷") {
			this.setState({
				beforeOperateValue: this.state.input,
				input: "",
				operator: "÷"
			})
			return

			// if(this.state.input === "") {
			// 	return null
			// }
		}

		// if(event === "=") {
		// 	this.state.beforeCalculateValue = this.state.input;

		// 	if(this.state.operator === "+"){
		// 		this.setState({input: parseInt(this.state.beforeOperateValue) + parseInt(this.state.beforeCalculateValue)})
		// 		return
		// 	}

		// 	else if(this.state.operator === "−"){
		// 		this.setState({input: parseInt(this.state.beforeOperateValue) - parseInt(this.state.beforeCalculateValue)})
		// 		return
		// 	}

		// 	else if(this.state.operator === "×"){
		// 		this.setState({input: parseInt(this.state.beforeOperateValue) * parseInt(this.state.beforeCalculateValue)})
		// 		return
		// 	}

		// 	else if(this.state.operator === "÷"){
		// 		this.setState({input: parseInt(this.state.beforeOperateValue) / parseInt(this.state.beforeCalculateValue)})
		// 		return
		// 	}
			
		// 	return 
		// }

		this.setState({
			input: this.state.input + event
		})
	}

	render() {
		return (
			<div className="app-container">
				<div className="calc-app">
					<div className="top-bar">
						<span>07:30</span>
						<img src={TopBar} alt="Top Icons" />
					</div>
					<div className="inputs">
						{this.state.input}
					</div>
					<div className="buttons">
						<Button name="AC" onButtonClicked={this.clickHandlerPressed} />
						<Button name="±" onButtonClicked={this.clickHandlerPressed} />
						<Button name="%" onButtonClicked={this.clickHandlerPressed} />
						<Button name="←" onButtonClicked={this.clickHandlerPressed} />
						<Button name="7" onButtonClicked={this.clickHandlerPressed} />
						<Button name="8" onButtonClicked={this.clickHandlerPressed} />
						<Button name="9" onButtonClicked={this.clickHandlerPressed} />
						<Button name="÷" onButtonClicked={this.clickHandlerPressed} />
						<Button name="4" onButtonClicked={this.clickHandlerPressed} />
						<Button name="5" onButtonClicked={this.clickHandlerPressed} />
						<Button name="6" onButtonClicked={this.clickHandlerPressed} />
						<Button name="×" onButtonClicked={this.clickHandlerPressed} />
						<Button name="1" onButtonClicked={this.clickHandlerPressed} />
						<Button name="2" onButtonClicked={this.clickHandlerPressed} />
						<Button name="3" onButtonClicked={this.clickHandlerPressed} />
						<Button name="−" onButtonClicked={this.clickHandlerPressed} />
						<Button name="." onButtonClicked={this.clickHandlerPressed} />
						<Button name="0" onButtonClicked={this.clickHandlerPressed} />
						<Button name="=" onButtonClicked={this.clickHandlerPressed} />
						<Button name="+" onButtonClicked={this.clickHandlerPressed} />
					</div>
					<div className="bottom">

					</div>
				</div>
			</div>
		)
	}
}

export default App;
